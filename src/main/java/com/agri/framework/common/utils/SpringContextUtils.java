package com.agri.framework.common.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class SpringContextUtils implements ApplicationContextAware {
    private static ApplicationContext applicationContext;

    public SpringContextUtils() {
    }

    public void setApplicationContext(ApplicationContext application) throws BeansException {
        applicationContext = application;
    }

    public static Object getBean(String beanName) {
        return applicationContext.getBean(beanName);
    }
}
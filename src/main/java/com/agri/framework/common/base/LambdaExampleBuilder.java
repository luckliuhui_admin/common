package com.agri.framework.common.base;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

@FunctionalInterface
public interface LambdaExampleBuilder<T> {
    void buildExample(LambdaQueryWrapper<T> var1);
}

package com.agri.framework.common.base;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.Date;
public class BaseController {
    @Autowired(
            required = false
    )
    protected BaseCacheManager baseCacheManager;
    protected final Snowflake snowflake = IdUtil.getSnowflake(1L, 1L);

    public BaseController() {
    }

    public static Page getPage(BaseVo model) {
        if (model.getPageNo() != null && model.getPageSize() != null) {
            Page page = new Page((long)Convert.toInt(model.getPageNo(), 1), (long)Convert.toInt(model.getPageSize(), 10));
            return page;
        } else {
            return null;
        }
    }

    public Object getUser() {
        HttpServletRequest request = this.getRequest();
        return this.getCacheValue("login_cache", request.getHeader("generator-userId"));
    }

    public Object getCacheValue(String cacheName, String key) {
        return this.getCacheValue(cacheName, key, false);
    }

    public Object getCacheValue(String cacheName, String key, boolean delete) {
        try {
            BaseCache cache = this.baseCacheManager.getCache(cacheName);
            Object o = cache.getValue(key);
            if (delete) {
                cache.evict(key);
            }

            return o;
        } catch (Exception var6) {
            var6.printStackTrace();
            return null;
        }
    }

    public HttpServletRequest getRequest() {
        HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
        return request;
    }

    protected String getIp(HttpServletRequest request) {
        String ip = request.getHeader("X-Forwarded-For");
        if (ip != null && !ip.isEmpty() && !"unKnown".equalsIgnoreCase(ip)) {
            int index = ip.indexOf(",");
            return index != -1 ? ip.substring(0, index) : ip;
        } else {
            ip = request.getHeader("X-Real-IP");
            if (ip != null && !ip.isEmpty() && !"unKnown".equalsIgnoreCase(ip)) {
                return ip;
            } else {
                ip = request.getHeader("Proxy-Client-IP");
                if (ip != null && !ip.isEmpty() && !"unKnown".equalsIgnoreCase(ip)) {
                    return ip;
                } else {
                    ip = request.getHeader("WL-Proxy-Client-IP");
                    if (ip != null && !ip.isEmpty() && !"unKnown".equalsIgnoreCase(ip)) {
                        return ip;
                    } else {
                        ip = request.getRemoteAddr();
                        return ip.equals("0:0:0:0:0:0:0:1") ? "127.0.0.1" : ip;
                    }
                }
            }
        }
    }

    protected Date addOneDate(Date reqEndTime) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(reqEndTime);
        calendar.add(5, 1);
        return calendar.getTime();
    }
}

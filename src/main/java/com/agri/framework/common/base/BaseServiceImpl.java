package com.agri.framework.common.base;

import com.agri.framework.common.result.Result;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

public class BaseServiceImpl<T extends Serializable, M extends CommonMapper<T>> extends ServiceImpl<M, T> implements BaseService<T, M> {
    public BaseServiceImpl() {
    }

    public Result getQueryResult(Page page, LambdaExampleBuilder<T> builder) {
        QueryWrapper<T> queryWrapper = new QueryWrapper();
        builder.buildExample(queryWrapper.lambda());
        if (page != null) {
            ((CommonMapper)this.baseMapper).selectPage(page, queryWrapper);
            return Result.data(page.getRecords(), page.getTotal());
        } else {
            return Result.data(((CommonMapper)this.baseMapper).selectList(queryWrapper));
        }
    }

    @Transactional(
            rollbackFor = {Exception.class}
    )
    public void insertBatch(List<T> dataList) {
        ((CommonMapper)this.baseMapper).insertBatch(dataList);
    }

    @Transactional(
            rollbackFor = {Exception.class}
    )
    public void insertBatch(List<T> dataList, LambdaQueryWrapper<T> deleteWrapper) {
        if (deleteWrapper != null) {
            ((CommonMapper)this.baseMapper).delete(deleteWrapper);
        }

        ((CommonMapper)this.baseMapper).insertBatch(dataList);
    }
}

package com.agri.framework.common.base;

import com.agri.framework.common.annotation.groups.DeleteMultiple;
import com.agri.framework.common.annotation.groups.SelectDate;
import com.agri.framework.common.annotation.groups.SelectPage;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
public class BaseVo implements Serializable{
    private static final long serialVersionUID = -3929999481707031326L;
    private @DecimalMin(
            value = "1",
            message = "分页条件pageNo必须为大于0的数",
            groups = {SelectPage.class}
    ) Integer pageNo = -1;
    private @DecimalMin(
            value = "1",
            message = "分页条件pageSize必须为大于0的数",
            groups = {SelectPage.class}
    ) Integer pageSize = -1;
    private @NotNull(
            message = "集合idsList不能为空",
            groups = {DeleteMultiple.class}
    ) List<Integer> idsList;
    private @NotNull(
            message = "开始时间不能为空",
            groups = {SelectDate.class}
    ) Date reqStartTime;
    private @NotNull(
            message = "结束时间不能为空",
            groups = {SelectDate.class}
    ) Date reqEndTime;

    public BaseVo() {
    }

    public Integer getPageNo() {
        return this.pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return this.pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public List<Integer> getIdsList() {
        return this.idsList;
    }

    public void setIdsList(List<Integer> idsList) {
        this.idsList = idsList;
    }

    public Date getReqStartTime() {
        return this.reqStartTime;
    }

    public void setReqStartTime(Date reqStartTime) {
        this.reqStartTime = reqStartTime;
    }

    public Date getReqEndTime() {
        return this.reqEndTime;
    }

    public void setReqEndTime(Date reqEndTime) {
        this.reqEndTime = reqEndTime;
    }
}

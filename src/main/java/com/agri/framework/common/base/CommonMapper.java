package com.agri.framework.common.base;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
public interface CommonMapper<T> extends BaseMapper<T> {
    int insertBatch(List<T> var1);
}

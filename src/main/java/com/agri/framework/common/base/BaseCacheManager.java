package com.agri.framework.common.base;

import org.springframework.cache.CacheManager;
import org.springframework.lang.Nullable;
public interface BaseCacheManager extends CacheManager {
    @Nullable
    BaseCache getCache(String var1);
}

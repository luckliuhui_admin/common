package com.agri.framework.common.base;

import org.springframework.cache.Cache;

import java.time.Duration;
public interface BaseCache extends Cache{
    void put(Object var1, Object var2, Duration var3);

    Object getValue(Object var1);
}

package com.agri.framework.common.base;

import com.agri.framework.common.result.Result;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.io.Serializable;
import java.util.List;
public interface BaseService <T extends Serializable, M extends CommonMapper<T>> extends IService<T> {
    Result getQueryResult(Page var1, LambdaExampleBuilder<T> var2);

    void insertBatch(List<T> var1);

    void insertBatch(List<T> var1, LambdaQueryWrapper<T> var2);
}

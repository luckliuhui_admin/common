package com.agri.framework.common.constant;

public class CacheConstant {
    public static final String CACHE_BEAN_NAME = "baseCacheManager";
    public static final String LOGIN_CACHE = "login_cache";
    public static final String KAPTCHA_CACHE = "kaptcha_cache";
    public static final String RSA_CACHE = "rsa_cache";
    public static final String RETRY_PASSWORD = "retry_password";
    public static final String JWT_CACHE = "jwt_cache";
    public static final String JWT_ACCESS_TOKEN = "jwt_access_token";
    public static final String JWT_REFRESH_TOKEN = "jwt_refresh_token";

    public CacheConstant() {
    }
}

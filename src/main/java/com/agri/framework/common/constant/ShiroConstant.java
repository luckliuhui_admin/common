package com.agri.framework.common.constant;

public class ShiroConstant {
    public static final Integer CODE_EXPIRE_TIME = 300;
    public static final String SHIRO_DEFAULT_JWT_TYPE = "jwt";
    public static final Long TOKEN_EXPIRE_TIME = 2678400000L;
    public static final String USER_ID = "userId";
    public static final String TOKEN_HEADER_NAME = "Authorization";
    public static final String TOKEN_HEADER_NAME_CLIENT = "certification-client";
    public static final String TOKEN_HEADER_USER_ID = "generator-userId";
    public static final String USER_AGENT_HEADER = "User-Agent";

}

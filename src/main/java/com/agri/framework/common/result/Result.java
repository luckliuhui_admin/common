package com.agri.framework.common.result;

import com.baomidou.mybatisplus.core.metadata.IPage;

import java.io.Serializable;
public class Result <T> implements Serializable {
    private static final long serialVersionUID = 1L;
    private String code;
    private boolean success;
    private T data;
    private String msg;
    private long total;

    public Result() {
    }

    private Result(IResultCode resultCode) {
        this(resultCode, null, resultCode.getMessage());
    }

    private Result(IResultCode resultCode, String msg) {
        this(resultCode, null, msg);
    }

    private Result(IResultCode resultCode, T data, String msg) {
        this(resultCode.getCode(), data, msg);
    }

    private Result(String code, T data, String msg) {
        this.code = code;
        this.data = data;
        this.msg = msg;
        this.success = ResultCode.SUCCESS.code.equals(code);
    }

    private Result(String code, T data, String msg, long total) {
        this.code = code;
        this.data = data;
        this.msg = msg;
        this.total = total;
        this.success = ResultCode.SUCCESS.code.equals(code);
    }

    public static Result success() {
        return new Result(ResultCode.SUCCESS, ResultCode.SUCCESS.message);
    }

    public static Result success(String msg) {
        return new Result(ResultCode.SUCCESS, msg);
    }

    public static Result fail(String msg) {
        return new Result(ResultCode.FAILURE, msg);
    }

    public static Result fail(IResultCode resultCode) {
        return new Result(resultCode);
    }

    public static Result fail(String code, String msg) {
        return new Result(code, (Object)null, msg);
    }

    public static Result fail(String code, String msg, Object data) {
        return new Result(code, data, msg);
    }

    public static Result fail(String msg, Object data) {
        return new Result(ResultCode.FAILURE, data, msg);
    }

    public static Result fail(IResultCode resultCode, Object data) {
        return new Result(resultCode.getCode(), data, resultCode.getMessage());
    }

    public static Result data(Object data) {
        return new Result(ResultCode.SUCCESS.code, data, ResultCode.SUCCESS.message);
    }

    public static Result data(IPage page) {
        return new Result(ResultCode.SUCCESS.code, page.getRecords(), ResultCode.SUCCESS.message, page.getTotal());
    }

    public static Result data(Object data, String msg) {
        return new Result(ResultCode.SUCCESS.code, data, msg);
    }

    public static Result data(Object data, long total) {
        return new Result(ResultCode.SUCCESS.code, data, ResultCode.SUCCESS.message, total);
    }

    public static Result failedMissToken() {
        return new Result(ResultCode.MISS_TOKEN);
    }

    public static Result tokenExpired() {
        return new Result(ResultCode.TOKEN_EXPIRED);
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isSuccess() {
        return this.success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public long getTotal() {
        return this.total;
    }

    public void setTotal(long total) {
        this.total = total;
    }
}

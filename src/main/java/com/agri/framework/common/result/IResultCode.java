package com.agri.framework.common.result;

import java.io.Serializable;

public interface IResultCode extends Serializable {
    String getMessage();

    String getCode();
}

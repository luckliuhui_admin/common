package com.agri.framework.common.result;

public enum ResultCode implements IResultCode {
    SUCCESS("200", "操作成功"),
    FAILURE("499", "操作失败"),
    PARAM_ERROR("400", "请求参数类型或格式错误"),
    UN_AUTHORIZED("401", "请求未授权"),
    NOT_FOUND("404", "当前接口地址不存在"),
    REQ_REJECT("403", "请求被拒绝"),
    METHOD_NOT_SUPPORTED("405", "不支持当前请求方法"),
    MEDIA_TYPE_NOT_SUPPORTED("415", "不支持当前媒体类型"),
    REQUEST_ERROR("444", "请检查接口请求地址或方式(GET/POST)是否正确"),
    SQL_INJECT_ERROR("445", "存在sql注入风险"),
    ENCRYPT_FAILURE("446", "加解密请求数据错误"),
    IMPORT_FAILURE("447", "导入失败"),
    GENERAL_FAILURE("500", "服务端错误"),
    TOKEN_EXPIRED("555", "access_token过期"),
    UNKNOWN_ERROR("999999", "系统繁忙，请稍后再试"),
    MISS_TOKEN("999", "Token失效，请重新登录");

    final String code;
    final String message;

    public String getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }

    private ResultCode(String code, String message) {
        this.code = code;
        this.message = message;
    }
}

